import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sympy as sp
from IPython.display import HTML

def latex(f, mode='inline', ln_notation=True):
    return sp.latex(f, mode, ln_notation)

def enable_math():
    """MathJax initialization for the current cell.

    This installs and configures MathJax for the current output.
    """
    try:
        display(HTML('''
            <script src="https://www.gstatic.com/external_hosted/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full,Safe&delayStartupUntil=configured"></script>
            <script>
                (() => {
                const mathjax = window.MathJax;
                mathjax.Hub.Config({
                'tex2jax': {
                    'inlineMath': [['$', '$']],
                    'displayMath': [['$$', '$$']],
                    'processEscapes': true,
                    'processEnvironments': true,
                    'skipTags': ['script', 'noscript', 'style', 'textarea', 'code'],
                    'displayAlign': 'center',
                },
                'HTML-CSS': {
                    'styles': {'.MathJax_Display': {'margin': 0}},
                    'linebreaks': {'automatic': true},
                    // Disable to prevent OTF font loading, which aren't part of our
                    // distribution.
                    'imageFont': null,
                },
                'messageStyle': 'none'
                });
                mathjax.Hub.Configured();
            })();
            </script>
            '''))
    except(NameError):
        pass


def dec2bin(value, verbose=False, tol=100):
    i_bin = "{0:b}".format(int(value))
    f_bin = ''
    f = value % 1
    for _ in range(tol):
        f = f*2
        if(f == 0):
            break
        if verbose: print(int(f), f)
        f_bin = f_bin+str(int(f))
        f = f % 1
    return i_bin+'.'+f_bin


def bin2dec(bin, verbose=False):
    bin = str(bin)
    bin = bin.split('.')
    integer = bin[0]
    fractional = bin[1] if len(bin) > 1 else ''
    exp = len(integer)
    dec = 0
    for i in integer:
        exp = exp - 1
        dec = dec + int(i)*2**exp
        if verbose: print(i+'*2^'+str(exp))

    for i in fractional:
        exp = exp - 1
        dec = dec + int(i)*2**exp
        if verbose: print(i+'*2^'+str(exp))
    return dec


def nbissec(a, b, tol):
    return int(np.ceil((np.log10(b-a)-np.log10(tol))/np.log10(2)))


def ezplot(f, x0=-4, x1=4, title=None, y0=None, y1=None, axis=True):
    x = np.linspace(x0, x1, 100)
    plt.plot(x, f(x))
    plt.grid(True)
    plt.grid(True)
    plt.xlabel('x')
    plt.ylabel('y')
    if title: plt.title(title)
    if axis:
        plt.axhline(0, color='k')
        plt.axvline(0, color='k')
    if y0: plt.ylim(bottom=3)  # adjust the top leaving bottom unchanged
    if y1: plt.ylim(top=1)  # adjust the bottom leaving top unchanged
    return plt


def bissec(f, a, b, tol, num_max_iteracoes=100):
    df = pd.DataFrame()
    num_iteracoes = 0
    if(f(a)*f(b) > 0):
        raise Exception('O intervalo [a, b] não contém uma raiz real')
    a_s = []
    b_s = []
    x_s = []
    erro_s = []
    while (num_iteracoes < num_max_iteracoes):
        num_iteracoes = num_iteracoes+1
        fa = f(a)
        x = (a+b)/2
        fx = f(x)
        a_s.append(a)
        b_s.append(b)
        x_s.append(x)
        if (fa*fx) < 0:
            b = x
        else:
            a = x
        erro_x = abs(b-a)
        erro_s.append(erro_x)
        if (erro_x < tol):
            break
    df['n'] = list(range(1, len(a_s)+1))
    df['a'] = a_s
    df['x'] = x_s
    df['b'] = b_s
    df['f(a)'] = df['a'].apply(f)
    df['f(x)'] = df['x'].apply(f)
    df['f(b)'] = df['b'].apply(f)
    df['erro'] = erro_s
    return x, erro_x, num_iteracoes, df


def fpos(f, a, b, tol, num_max_iteracoes=100):
    df = pd.DataFrame()
    a_s = []
    b_s = []
    x_s = []
    erro_s = []
    num_iteracoes = 0
    if(f(a)*f(b) > 0):
        raise Exception('O intervalo [a, b] não contém uma raiz real')
    while (num_iteracoes < num_max_iteracoes):
        num_iteracoes = num_iteracoes+1
        fa = f(a)
        fb = f(b)
        x = (a*fb-b*fa)/(fb-fa)
        fx = f(x)
        a_s.append(a)
        b_s.append(b)
        x_s.append(x)
        if (fa*fx) < 0:
            b = x
        else:
            a = x
        erro_x = abs(b-a)
        erro_s.append(erro_x)
        if (erro_x < tol or abs(fx) < tol):
            break
    df['n'] = list(range(1, len(a_s)+1))
    df['a'] = a_s
    df['x'] = x_s
    df['b'] = b_s
    df['f(a)'] = df['a'].apply(f)
    df['f(x)'] = df['x'].apply(f)
    df['f(b)'] = df['b'].apply(f)
    df['erro'] = erro_s
    return x, erro_x, num_iteracoes, df


def pfixo(f, g, x0, tol, num_max_iteracoes=100):
    df = pd.DataFrame()
    x_s = []
    erro_s = []
    num_iteracoes = 1
    x_anterior = x0
    while ((num_iteracoes < num_max_iteracoes)):
        x = g(x_anterior)
        x_s.append(x)
        erro_x = abs(x-x_anterior)
        erro_s.append(erro_x)
        if (erro_x < tol or abs(f(x)) < tol):
            break
        num_iteracoes = num_iteracoes + 1
        x_anterior = x
    df['n'] = list(range(1, len(x_s)+1))
    df['x'] = x_s
    df['f(x)'] = df['x'].apply(f)
    df['erro'] = erro_s
    return x, erro_x, num_iteracoes, df


def newton(f, fl, x0, tol, num_max_iteracoes=100):
    df = pd.DataFrame()
    x_s = []
    erro_s = []
    num_iteracoes = 1
    x_anterior = x0
    while ((num_iteracoes < num_max_iteracoes)):
        x = x_anterior - f(x_anterior)/fl(x_anterior)
        x_s.append(x)
        erro_x = abs(x-x_anterior)
        erro_s.append(erro_x)
        if (erro_x < tol or abs(f(x)) < tol):
            break
        num_iteracoes = num_iteracoes + 1
        x_anterior = x
    df['n'] = list(range(1, len(x_s)+1))
    df['x'] = x_s
    df['f(x)'] = df['x'].apply(f)
    df['erro'] = erro_s
    return x, erro_x, num_iteracoes, df


def secante(f, x0, x1, tol, num_max_iteracoes=100):
    d = pd.DataFrame()
    x_s = []
    erro_s = []
    # Cálculos iniciais
    num_iteracoes = 1
    fx0 = f(x0)
    fx1 = f(x1)
    while num_iteracoes < num_max_iteracoes:
        # Calcular próxima aproximação
        x = x1 - (x1-x0)*fx1/(fx1-fx0)
        x_s.append(x)
        # Teste do critério de parada
        erro_x = abs(x-x1)
        erro_s.append(erro_x)
        if (erro_x < tol or abs(f(x)) < tol):
            break
        # Preparação para a próxima iteração
        num_iteracoes = num_iteracoes + 1
        x0 = x1
        x1 = x
        fx0 = fx1
        fx1 = f(x)
    d['n'] = list(range(1, len(x_s)+1))
    d['x'] = x_s
    d['f(x)'] = d['x'].apply(f)
    d['erro'] = erro_s
    return x, erro_x, num_iteracoes, d


def LU(A):
    U = np.copy(A).astype('double')
    n = np.shape(U)[0]
    L = np.eye(n)
    for j in np.arange(n-1):
        for i in np.arange(j+1, n):
            L[i, j] = U[i, j]/U[j, j]
            for k in np.arange(j+1, n):
                U[i, k] = U[i, k] - L[i, j]*U[j, k]
                U[i, j] = 0
    return L, U


def jacobi(A, b, x0=None, tol=np.finfo(float).eps,
           num_max_iteracoes=100,
           verbose=False):
    # Inicialmente garantimos que todas as entradas estejam em um formato
    # compatível. "np.array(A)" transforma a entrada em um array do numpy
    # (formato normalmente usado para representar matrizes). "astype('float')"
    # garante que estejamos trabalhando com números em ponto flutuante.
    A = np.array(A).astype('float')
    b = np.array(b).astype('float')
    if x0 is None:
        x0 = np.zeros(b.shape)
    else:
        x0 = np.array(x0).astype('float')
    # Valores iniciais
    num_equacoes = np.shape(A)[0]
    x = np.copy(x0)
    num_iteracoes = 0
    # Iterações
    while (num_iteracoes < num_max_iteracoes):
        num_iteracoes = num_iteracoes+1
        for i in np.arange(num_equacoes):
            x[i, 0] = b[i, 0]
            for j in np.concatenate((np.arange(0, i), np.arange(i+1, num_equacoes))):
                x[i, 0] -= A[i, j]*x0[j, 0]
            x[i, 0] /= A[i, i]
        if verbose: print(x.reshape([x.shape[0], ]))
        # Calculo do erro
        if (np.linalg.norm(x-x0, np.inf) < tol):
            return x, num_iteracoes
        # Prepara nova iteração
        x0 = np.copy(x)
    raise NameError('num. max. de iteracoes excedido.')


def seidel(A, b, x0=None, tol=np.finfo(float).eps,
           num_max_iteracoes=100,
           verbose=False):
    # Inicialmente garantimos que todas as entradas estejam em um formato
    # compatível. "np.array(A)" transforma a entrada em um array do numpy
    # (formato normalmente usado para representar matrizes). "astype('float')"
    # garante que estejamos trabalhando com números em ponto flutuante.
    A = np.array(A).astype('float')
    b = np.array(b).astype('float')
    if x0 is None:
        x0 = np.zeros(b.shape)
    else:
        x0 = np.array(x0).astype('float')
    # Valores iniciais
    num_equacoes = np.shape(A)[0]
    x = np.copy(x0)
    num_iteracoes = 0
    # Iterações
    while (num_iteracoes < num_max_iteracoes):
        num_iteracoes = num_iteracoes+1
        for i in np.arange(num_equacoes):
            x[i, 0] = b[i, 0]
            for j in np.concatenate((np.arange(0, i), np.arange(i+1, num_equacoes))):
                x[i, 0] -= A[i, j]*x[j, 0]
            x[i, 0] /= A[i, i]
        if verbose: print(x.reshape([x.shape[0], ]))
        # Calculo do erro
        if (np.linalg.norm(x-x0, np.inf) < tol):
            return x, num_iteracoes
        # Prepara nova iteração
        x0 = np.copy(x)
    raise NameError('num. max. de iteracoes excedido.')


def linhas(A):
    A = np.array(A).astype('float')
    lines = A.shape[0]
    criteria = True
    for i in range(lines):
        if abs(A[i, i]) <= np.sum(abs(np.delete(A[i, :], i))):
            criteria = False
            break
    return criteria


def trapesio(f, a, b, n):
    h = (b-a)/n
    x = np.linspace(a, b, n+1)
    fx = f(x)
    m = np.ones(n+1)
    m[1:n] = 2
    I = f(x)*m
    df = pd.DataFrame()
    df['x'] = x
    df['f(x)'] = fx
    df['m'] = m
    df['f(x)*m'] = I
    return I.sum()*h/2, df


def simpson(f, a, b, n):
    h = (b-a)/n
    x = np.linspace(a, b, n+1)
    fx = f(x)
    m = np.ones(n+1)
    m[1:n:2] = 4
    m[2:n-1:2] = 2
    I = f(x)*m
    df = pd.DataFrame()
    df['x'] = x
    df['f(x)'] = fx
    df['m'] = m
    df['f(x)*m'] = I
    return I.sum()*h/3, df


def lagrange(x, y):
    x_ = sp.symbols('x')
    L = []
    for _ in x:
        L.append(sp.sympify(1))
    for k in range(len(x)):
        for i in range(len(x)):
            if (i != k):
                L[k] = L[k]*(x_-x[i])/(x[k]-x[i])
    P = sp.sympify(0)
    for i in range(len(x)):
        P = P + y[i]*L[i]
    return P
