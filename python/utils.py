def easy_plot(f, min=-6.28, max=6.28, n_points=100):
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    try:
        import mpld3
    except:
        !pip install mpld3
        import mpld3
    mpld3.enable_notebook()
  
    x = np.linspace(min,max, n_points)
    y = f(x)
    plt.plot(x, y)
