# clc limpa a tela
clc;
# clear limpa a memoria
clear;

# Dado o sistema Ax=b
A = [-3 1 1;
      2 5 1;
      2 3 7];  
b = [  2;
       5;
     -17];  

# x0 eh a aproximação inicial de x 
x0= [ 1;
      1;
     -1];

# Resolucao do sistema:

# 1. Usando \

printf('1. Usando x=A\\b\n')
x = A\b

# 2. Usando o metodo de Jacobi com tolerancia de 10^-10 e numero maximo de 100 iteracoes
disp('2. Usando Jacobi')
tolerancia = 10^-10;
numero_maximo_de_iteracoes=100;
[x, numero_de_iteracoes] = jacobi(A,b,x0,tolerancia,numero_maximo_de_iteracoes)

# 3. Usando o metodo de Gauss-Seidel com tolerancia de 10^-10 e numero maximo de 100 iteracoes
disp('2. Usando Gauss-Seidel')
[x, numero_de_iteracoes] = gauss_seidel(A,b,x0,tolerancia,numero_maximo_de_iteracoes)
