function [convergence, beta] = linhas(A)
  [m n]=size(A);
  beta=zeros(m,1);
  for i=1:m
    line = A(i, :);
    k = line(i);
    line(i) = [];
    beta(i)=sum(abs(line))/k;
  endfor;
  convergence = (max(abs(beta)) < 1);
endfunction
