function [convergence, beta]=sassenfeld(A)
  [m n]=size(A);
  beta=zeros(m,1);
  for i=1:m
    for j=1:i-1
      beta(i)=beta(i)+abs(A(i,j))/abs(A(i,i))*beta(j);
    endfor;
    for j=i+1:n
      beta(i)=beta(i)+abs(A(i,j))/abs(A(i,i));
    endfor;
  endfor;
  convergence = max(abs(beta)) < 1;
endfunction
