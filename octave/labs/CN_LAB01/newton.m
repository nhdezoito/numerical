%-----------------------------------------------------------------------
%             -------------                             -------
%             function file                             newton.m
%             -------------                             -------
%-----------------------------------------------------------------------
%                        ----------------------------
%                        Zeros Reais de Funcoes Reais
%                        ----------------------------
%                            --------------------
%                              Metodo de Newton
%                            --------------------

% Chamada do Procedimento  newton
%------------------------------------------
function [] = newton( funcao, derivada, x, tol )
%
%      Variaveis de Entrada
%---------------------------
% funcao: Identificador da funcao (retorna a funcao ponto)
% derivada: Identificador da derivada da funcao (retorna a derivada no ponto)
% x     : Aproximacao inicial para um zero da funcao
% tol   : Precisao da Aproximacao do zero da funcao
%
%       Variaveis de Saida
%-------------------------
%       x     :       Aproximacao do zero da funcao 
%       abs(f):       Valor absoluto da funcao no ponto x
%       errox :       Erro da Aproximacao do zero da funcao
%       iter  :       Numero de Iteracoes realizadas
%
%----------------------------
%    Metodo de Newton
%----------------------------
iter = 0;
limax =  50;
f = funcao( x );
d = derivada(x);
xold=x;
while (( abs(f)>tol ) && (iter<limax)) 
   x = x - f/d; 
   f = funcao( x );
   d = derivada(x);
   errox = abs(x - xold);
   iter=iter+1;
   xold=x;
   end;
%
% ----------------------------
%  Impressao dos resultados
% ----------------------------
%
disp('----------------------------');
disp('   Metodo de Newton');
disp('----------------------------');
sx=sprintf('x(raiz)=%e',x);
disp(sx);
sf=sprintf('|f(x)|=%e',abs(f));
disp(sf);
serro=sprintf('Erro em x=%e',errox);
disp(serro);
siter=sprintf('Numero de iteracoes=%d',iter);
disp(siter);
disp('----------------------------');
end;
 