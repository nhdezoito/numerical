%-----------------------------------------------------------------------
%             -------------                             -------
%             function file                             bissec.m
%             -------------                             -------
%-----------------------------------------------------------------------
%                        ----------------------------
%                        Zeros Reais de Funcoes Reais
%                        ----------------------------
%                            --------------------
%                            Metodo da Bisseccao
%                            --------------------

% Chamada do Procedimento  bissec
%------------------------------------------
function [] = bissec(funcao, a, b, tol )
%
%      Variaveis de Entrada
%---------------------------
% funcao: Identificador da funcao, retorna valor da funcao no ponto
% a,b   : Intervalo que contem um unico zero da funcao
% tol   : Precisao da Aproximacao do zero da funcao
%
%       Variaveis de Saida
%-------------------------
%       x     :       Aproximacao do zero da funcao em [a , b]
%       abs(f):       Valor absoluto da funcao no ponto x
%       errox :       Erro da Aproximacao do zero da funcao
%       iter  :       Numero de Iteracoes realizadas

%----------------------------
%    Metodo da Bisseccao
%----------------------------
iter = 0;
limax =  50;
errox = ( b - a );
s = sign( funcao( a ) );
f = funcao( b );
if ( s*f >= 0 ) 
   disp(' Erro em Bisseccao: extremos do intervalo nao tem sinal oposto'); 
   return; 
end;

x = ( a + b )/2;
f = funcao( x ); 

while ((abs(f)>tol) && (iter<limax))
   if( f*s>0 )
     a=x; 
   else
     b=x; 
   end;
   x = ( a + b )/2; 
   f = funcao( x );
   iter = iter + 1;
   errox = ( b - a );
end;
%
% ----------------------------
%  Impressao dos resultados
% ----------------------------
%
disp('----------------------------');
disp('   Metodo da Bisseccao');
disp('----------------------------');
sx=sprintf('x(raiz)=%e',x);
disp(sx);
sf=sprintf('|f(x)|=%e',abs(f));
disp(sf);
serro=sprintf('Erro em x=%e',errox);
disp(serro);
siter=sprintf('Numero de iteracoes=%d',iter);
disp(siter);
disp('----------------------------');
end;
      
