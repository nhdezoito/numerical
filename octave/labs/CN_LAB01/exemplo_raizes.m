clear; % limpa a memoria
clc; % limpa a tela
format long; % exibe os resultados com maior precisao
f = @(x) 4*x^5 - 48*x^3 + 191*x - 252 % funcao f(x)
df = @(x) 20*x^4 - 144*x^2 + 191 % derivada de f(x)
coeficientes = [4 0 -48 0 191 -252] % coeficeintes de f(x)
tolerancia = eps % Precisao da Aproximacao do zero da funcao

x_roots = roots(coeficientes) % calculo das raizes usando a funcao root do octave
x_fzero = fzero(f, [3 4]) % calculo da raiz no intervalo [3, 4] usando a funcao fzero do octave

bissec(f, 3, 4, tolerancia) % calculo da raiz no intervalo [3, 4] usando o metodo da bissecao
newton(f, df, 3, tolerancia) % calculo da raiz usando o metodo de newton
secante(f, 3, 4, tolerancia) % calculo da raiz no intervalo [3, 4] usando o metodo da secante
