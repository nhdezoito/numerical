%-----------------------------------------------------------------------
%             -------------                             -------
%             function file                             secante.m
%             -------------                             -------
%-----------------------------------------------------------------------
%                        ----------------------------
%                        Zeros Reais de Funcoes Reais
%                        ----------------------------
%                            --------------------
%                              Metodo da Secante
%                            --------------------

% Chamada do Procedimento Secante
%------------------------------------------
function [] = secante( funcao, x, x1, tol )
%
%      Variaveis de Entrada
%---------------------------
% funcao: Identificador da funcao, retorna o valor da funcao no ponto
% x, x1 : Aproximacoes iniciais para um zero da funcao
% tol   : Precisao da Aproximacao do zero da funcao
%
%       Variaveis de Saida
%-------------------------
%       x     :       Aproximacao do zero da funcao 
%       abs(f):       Valor absoluto da funcao no ponto x
%       errox :       Erro da Aproximacao do zero da funcao
%       iter  :       Numero de Iteracoes realizadas
%
%----------------------------
%    Metodo da Secante
%----------------------------
iter = 0;
limax =  50;
f  = funcao( x ); 
f1 = funcao( x1 ); 
iter=0;
xold=x1;
while (( abs(f)>tol ) && (iter<limax))
   x2 = x - f*(x-x1)/(f-f1); 
   x = x1;
   x1 = x2;
   f = f1;
   f1 = funcao( x1 );
   errox = abs(x - xold);
   iter=iter+1; 
   xold=x;
end;
%
% ----------------------------
%  Impressao dos resultados
% ----------------------------
%
disp('----------------------------');
disp('   Metodo da Secante');
disp('----------------------------');
sx=sprintf('x(raiz)=%e',x);
disp(sx);
sf=sprintf('|f(x)|=%e',abs(f));
disp(sf);
serro=sprintf('Erro em x=%e',errox);
disp(serro);
siter=sprintf('Numero de iteracoes=%d',iter);
disp(siter);
disp('----------------------------');
end;
