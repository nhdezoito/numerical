clear;
clc;

# Exemplo 1:
#Ajuste os dados abaixo para as curvas: 
# a) Uma reta phi=a1 + a2*x
# b) Um polin�mio de grau 2 phi=a1 + a2*x + a3*x^2
# c) Plote em um mesmo eixo as curvas de cada polin�mio
#    e os pontos tabelados.

x = [1 2 3 4 5 6 7 8];
y = [.5 .6 .9 .8 1.2 1.5 1.7 2];

# A fun��o polyfit faz o ajuste de curvas pelo m�todo
# dos m�nimos quadrados para polin�mios.
phi1 = polyfit(x,y,1);
phi2 = polyfit(x,y,2);

# Linspace � usado para criar um conjunto de pontos x
# entre os pontos inicial e final dos pontos x tabelados.
x1 = linspace(min(x),max(x));
y1 = polyval(phi1,x1);
y2 = polyval(phi2,x1);

# figure cria uma nova �rea de plotagem.
figure
# plot plota um gr�fico x,y. O par�metro 'o' faz que os
# pontos sejam plotados como pontos individuais.
plot(x,y,'o')
# hold on permite que v�rios gr�ficos sejam plotados em
# um mesmo eixo.
hold on;
plot(x1,y1)
plot(x1,y2)
# grid on adiciona linhas de grade ao gr�fico. 
grid on;
# legend adiciona legendas ao gr�fico
legend('Pontos tabelados','Reta phi=a1 + a2*x','Parabola phi=a1 + a2*x + a3*x^2')
# print salva o gr�fico atual
print -dpng exemplo_1.png
