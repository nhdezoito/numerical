clear;
clc;
x = [0.1   0.2   0.4  0.6   0.8   0.9];
y = [0.905 0.819 0.67 0.549 0.449 0.407];

# A fun��o polyfit faz o ajuste de curvas pelo m�todo
# dos m�nimos quadrados para polin�mios. Quando o grau do polin�mio
# � n_pontos-1 o resultado � equivalente ao da interpola��o.
P1 = polyfit(x,y,length(x)-1)

# Linspace � usado para criar um conjunto de pontos x
# entre os pontos inicial e final dos pontos x tabelados.
x1 = linspace(0,1);
y1 = polyval(P1,x1);
polyval(P1,0.432)

# figure cria uma nova �rea de plotagem.
figure
# plot plota um gr�fico x,y. O par�metro 'o' faz que os
# pontos sejam plotados como pontos individuais.
plot(x,y,'o')
# hold on permite que v�rios gr�ficos sejam plotados em
# um mesmo eixo.
hold on;
plot(x1,y1)
# grid on adiciona linhas de grade ao gr�fico. 
grid on;
# legend adiciona legendas ao gr�fico
legend('Pontos tabelados','Polinomio interpolador')
# print salva o gr�fico atual
print -dpng interpolacao_1_a.png
