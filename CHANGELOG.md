18.6.18
====
- Terceira lista contendo trabalhos sobre interpolação e ajuste de curvas
- Adicionada funções para a verificação dos critérios das linhas e de sasenfeld

18.5.6
====
- Segunda lista contendo trabalhos sobre sistemas de equações lineares

18.2.1
====
- Primeira lista contendo trabalhos sobre raizes reais.